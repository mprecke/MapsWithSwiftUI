//
//  ContentView.swift
//  MapsWithSwiftUI
//
//  Created by Moritz Philip Recke on 26.02.21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
           MapView()
             .tabItem {
                Image(systemName: "map.fill")
                Text("Simple Map")
              }
            CustomMapView()
              .tabItem {
                 Image(systemName: "plus.circle.fill")
                 Text("Annotations")
               }
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
