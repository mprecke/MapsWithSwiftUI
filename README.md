# MapKit with SwiftUI

This is a simple implementation of MapKit with SwiftUI. 

It is based on the MapKit integration within SwiftUI presented at WWDC 2020 and does not use any UIViewRepsentable or other UIKit elements to showcase the limited but easy to implement Map features available in SwiftUI.
